"""ClosestToZero modul"""


class ClosestToZero:
    """ClosestToZero class"""

    def __init__(self):
        pass

    @staticmethod 
    def closest_to_zero(input_list):
        if not isinstance(input_list, list):
            raise ValueError("NotValidInputList", input_list)
        else:
            closest = input_list[0]
            for i in input_list:
                number = i 
                absNumber = abs(number)
                absClosest = abs(closest)
                if (absNumber < absClosest):
                    closest = number
                elif(absNumber == absClosest & closest < 0):
                    closest = number
            return closest
     