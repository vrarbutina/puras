# Feature Closest to zero

As a customer 
I want to enter a list of positive and negative numbers
So that I could return number closest to zero

## Scenario_1

**Given**: List of positive numbers
**When**: The list is entered
**Then**: The result is closest positive number to zero

## Scenario_2

**Given**: List of negative numbers
**When**: The list is entered
**Then**: The result is closest negative number to zero

## Scenario_3

**Given**: List of positive and negative numbers
**When**: The list is entered
**Then**: The result is closest positive number to zero

## Scenario_4

**Given**: List of positive and negative numbers
**When**: The list is entered
**Then**: The result is closest negative number to zero
