"""Test modul for ClosestToZero class"""

import unittest

from closest_to_zero import ClosestToZero


class TestClosestToZero(unittest.TestCase):
    """Test class for ClosestToZero class"""

    def test_basic(self):
     self.assertEqual(1, 1)

    def test_should_return_number_closest_to_zero_from_positive_numbers_list(self):
        """Testing Scenario_1, List with positive numbers as an input"""
        input_list = [4, 5, 2, 8, 7, 1]
        self.assertEqual(ClosestToZero.closest_to_zero(input_list), 1)

    def test_should_return_number_closest_to_zero_from_negative_numbers_list(self):
        """Testing Scenario_2, List with negative numbers as an input"""
        input_list = [-4, -5, -2, -8, -7, -1]
        self.assertEqual(ClosestToZero.closest_to_zero(input_list), -1)

    def test_should_return_number_closest_to_zero_from_positive_and_negative_numbers_list(self):
        """Testing Scenario_3, List with positive and negative numbers as an input"""
        input_list = [-4, 5, 2, -8, 7, 1, 3]
        self.assertEqual(ClosestToZero.closest_to_zero(input_list), 1)

    def test_should_return_number_closest_to_zero_from_positive_and_negative_numbers_list_return_negative(self):
        """Testing Scenario_3, List with positive and negative numbers as an input"""
        input_list = [-4, 5, 2, -8, 7, -1, 3]
        self.assertEqual(ClosestToZero.closest_to_zero(input_list), -1)


unittest.main()
